﻿using System.ComponentModel.DataAnnotations;

namespace BankSimulation.DataModel.RequestModel
{
    public class TransactionIdRequest
    {
        [Required]
        [RegularExpression("^[0-9A-Z]{26}$",ErrorMessage ="Transaction ID should be ony Capital Letters and numeric of 26 length ")]
        public string TransactionId { get; set; }=null!;
    }
}
