﻿using System.ComponentModel.DataAnnotations;

namespace BankSimulation.DataModel.RequestModel
{
    public class AddBranchRequest
    {
        [Required]
        public string Branchname { get; set; } = null!;
        [Required]
        public int Bcategory { get; set; } 
    }
    public class EditBranchRequest
    {
        [Required]
        public int BranchId { get; set; }
        [Required]
        public string Branchname { get; set; } = null!;
        [Required]
        public int Bcategory { get; set; }
        public int workstatus { get; set; } = 0;

    }
}
