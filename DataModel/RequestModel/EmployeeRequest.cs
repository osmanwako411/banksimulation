﻿using BankSimulation.DataModel.DatabaseModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BankSimulation.DataModel.RequestModel
{
    public class EmployeeRequest
    {
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Employee First Name should be 2-20 characters")]
        [RegularExpression("^[A-Za-z]+$", ErrorMessage = "Name should be only english alphabet.")]
        public string firstname { get; set; } = string.Empty;
        [Required]
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Employee second Name should be 2-20 characters")]
        [RegularExpression("^[A-Za-z]+$", ErrorMessage = "Name should be only english alphabet.")]
        public string secondname { get; set; } = string.Empty;
        [Required]
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Employee Last Name should be 2-20 characters")]
        [RegularExpression("^[A-Za-z]+$", ErrorMessage = "Name should be only english alphabet.")]
        public string lastname { get; set; } = string.Empty;
        [Required]
        public int branchid { get; set; }
        [Required]
        [RegularExpression("^(?:male|Male|female|Female)$", ErrorMessage = "Gender Should be only Male(male) or Female(female)")]
        public string gender { get; set; } = "Male";
        [Required]
        [StringLength(18)]
        [RegularExpression("^(?:Permanent|Contract|Probation)$", ErrorMessage = "Employement Type can be only Permanent,Probation or Contract")]
        public string etype { get; set; } = "Probation";
        [StringLength(13, MinimumLength = 10, ErrorMessage = "Phone Numbere should be 10-13 digits")]
        [RegularExpression("^[0-9\\+]+$", ErrorMessage = "Phone Number should contain Only number and (+) ")]
        public string phone { get; set; } = null!;
        [Required]
        [RegularExpression("^[0-9\\.]+$", ErrorMessage = "Salary can be only number.")]
        public double salary { get; set; }
        [Required]
        public DateTime birthdate { get; set; }
        public int erole { get; set; }
    }
    public class EditEmployeeProfile {
        [Required]
        public int employeeid { get; set; }
        [Required]
        public String photo { get; set; }=null!;
    }
}
