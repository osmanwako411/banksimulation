﻿using BankSimulation.DataModel.DatabaseModel;
using System.ComponentModel.DataAnnotations;
using System.Reflection.Metadata;

namespace BankSimulation.DataModel.RequestModel
{
    public class CustomerRequest
    {
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Employee First Name should be 2-20 characters")]
        [RegularExpression("^[A-Za-z]+$", ErrorMessage = "Name should be only english alphabet.")]
        public string FirstName { get; set; } = string.Empty;
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Employee First Name should be 2-20 characters")]
        [RegularExpression("^[A-Za-z]+$", ErrorMessage = "Name should be only english alphabet.")]
        public string MiddleName { get; set; } = string.Empty;
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Employee First Name should be 2-20 characters")]
        [RegularExpression("^[A-Za-z]+$", ErrorMessage = "Name should be only english alphabet.")]
        public string LastName { get; set; } = string.Empty;
        [Required]
        [RegularExpression("^(?:male|Male|female|Female)$", ErrorMessage = "Gender should be only Male(male) or Female(female)")]
        public string Gender { get; set; } = "Male";
        [Required]
        [StringLength(13, MinimumLength = 10, ErrorMessage = "Phone Numbere should be 10-13 digits")]
        [RegularExpression("^[0-9\\+]+$", ErrorMessage = "Phone Number should contain Only number and (+) ")]
        public string phone { get; set; } = null!;
        [Required]
        public int BranchId { get; set; }
        [Required]
        [Range(0.0, 20000000.00)]
        public double Deposit { get; set; } = 0.00;
    }
    public class Editcustomerprofile{
        [Required]
        public long CustomerId { get; set; }
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Employee First Name should be 2-20 characters")]
        [RegularExpression("^[A-Za-z]+$", ErrorMessage = "Name should be only english alphabet.")]
        public string FirstName { get; set; } = string.Empty;
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Employee First Name should be 2-20 characters")]
        [RegularExpression("^[A-Za-z]+$", ErrorMessage = "Name should be only english alphabet.")]
        public string MiddleName { get; set; } = string.Empty;
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Employee First Name should be 2-20 characters")]
        [RegularExpression("^[A-Za-z]+$", ErrorMessage = "Name should be only english alphabet.")]
        public string LastName { get; set; } = string.Empty;
        [Required]
        [RegularExpression("^(?:male|Male|female|Female)$", ErrorMessage = "Gender can be only Male(male) or Female(female)")]
        public string Gender { get; set; } = "Male";
        [Required]
        [StringLength(13, MinimumLength = 10, ErrorMessage = "Phone Numbere should be 10-13 digits")]
        [RegularExpression("^[0-9\\+]+$", ErrorMessage = "Phone Number should contain Only number and (+) ")]
        public string phone { get; set; } = null!;
        [Required]
        public int BranchId { get; set; }
    }
    public class Editcustomeraccount
    {
        [Required]
         public long CustomerId { get; set; }
        public String? Photo { get; set; } = null;
        public String? Signature { get; set; } = null;
        public bool? status { get; set; } = false;
    }
}
