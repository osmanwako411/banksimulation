﻿using BankSimulation.DataModel.DatabaseModel;

namespace BankSimulation.DataModel.ResponseModel
{
    public class ResponseTransaction:Message
    {
        public long CustomerId { get; set; } = 0;
        public string AccountNo { get; set; }=String.Empty;
        public double CurrentBalance { get; set; } = 0;
        public string FirstName { get; set; } = string.Empty;
        public string MiddleName { get; set; } = string.Empty;
        public string LastName { get; set; } = string.Empty;
        public string Gender { get; set; } = String.Empty;
        public string? Phone { get; set; }
        public Byte[]? Photo { get; set; }
        public Byte[]? Signature { get; set; }
        public  Branchtable? Branch { get; set; }
        public bool status { get; set; } = false;
        public DateTime createddate { get; set; }= DateTime.Now;

    }
}
