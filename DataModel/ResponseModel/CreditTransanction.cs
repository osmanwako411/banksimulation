﻿namespace BankSimulation.DataModel.ResponseModel
{
    public class CreditTransanction
    {
        public string TransactionId { get; set; } = null!;
        public long CustomerId { get; set; }
        public string AccountNo { get; set; } = null!;
        public string AccountStatus { get; set; } = "Active";
        public string FullName { get; set; } = String.Empty;
        public string Gender { get; set; } = "Male";
        public double CreditAmount { get; set; } = 0;
        public string TransactionType { get; set; } = "Deposit";
        public double PreviousBalance { get; set; } = 0;
        public double CurrentBalance { get; set; } = 0;
        public DateTime? TransactionDate { get; set; }

    }
}
