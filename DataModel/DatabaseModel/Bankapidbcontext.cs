﻿using System;
using Microsoft.EntityFrameworkCore;
namespace BankSimulation.DataModel.DatabaseModel
{
    public class Bankapidbcontext : DbContext
    {
        public Bankapidbcontext()
        {
        }

        public Bankapidbcontext(DbContextOptions<Bankapidbcontext> options)
            : base(options)
        {
        }
        public virtual DbSet<TransactionHistorytable> TransactionHistorytables { get; set; } = null!;
        public virtual DbSet<Customertable> Customertables { get; set; } = null!;
        public virtual DbSet<CustomerHistorytable> CustomerHistorytables { get; set; } = null!;
        public virtual DbSet<Transactiontable> Transactiontables { get; set; } = null!;
        public virtual DbSet<Employeetable> Employeetables { get; set; } = null!;
        public virtual DbSet<Branchtable> Branchtables { get; set; } = null!;
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }
      protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Customertable>().HasIndex(C=>C.phone).IsUnique();
            modelBuilder.Entity<Employeetable>().HasIndex(C => C.phone).IsUnique();
            modelBuilder.Entity<CustomerHistorytable>().HasIndex(C => C.CustomerId).IsUnique();
        }
    }
}
