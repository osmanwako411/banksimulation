﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BankSimulation.DataModel.DatabaseModel
{
    public class CustomerHistorytable
    {
        [Key]
        public long CustomerHistoryId { get; set; }
        [Required]
        public long CustomerId { get; set; }
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Employee First Name should be 2-20 characters")]
        [RegularExpression("^[A-Za-z]+$", ErrorMessage = "Name should be only english alphabet.")]
        public string FirstName { get; set; }=string.Empty;
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Employee First Name should be 2-20 characters")]
        [RegularExpression("^[A-Za-z]+$", ErrorMessage = "Name should be only english alphabet.")]
        public string MiddleName { get; set; } = string.Empty;
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Employee First Name should be 2-20 characters")]
        [RegularExpression("^[A-Za-z]+$", ErrorMessage = "Name should be only english alphabet.")]
        public string LastName { get; set; }=string.Empty ;
        [Required]
        [RegularExpression("^(?:male|Male|female|Female)$", ErrorMessage = "Gender should be only Male(male) or Female(female)")]
        public string Gender { get; set; } = "Male";
        [StringLength(13, MinimumLength = 10, ErrorMessage = "Phone Numbere should be 10-13 digits")]
        [RegularExpression("^[0-9\\+]+$", ErrorMessage = "Phone Number should contain Only number and (+) ")]
        public string? phone { get; set; } = null;
        public Byte[]? Photo { get; set; } = null;
        public Byte[]? Signature { get; set; } = null;
        public int BranchId { get; set; }
        [ForeignKey("BranchId")]
        public virtual Branchtable Branch { get; set; }=null!;
        public Transactiontable Book { get; set; } = null!;
        public bool status { get; set; }=false;
        [Required]
        public DateTime createddate { get; set; }
        public DateTime Deactiveddate { get; set; }=DateTime.Now;

    }
}
