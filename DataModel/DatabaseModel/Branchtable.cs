﻿
using System.ComponentModel.DataAnnotations;

namespace BankSimulation.DataModel.DatabaseModel
{
    public class Branchtable
    {
        [Required]
        [Key]
        public int BranchId { get; set; }
        [Required]
        public string Branchname { get; set; } = string.Empty;
        [Required]
        public BranchCategory Bcategory { get; set; } = BranchCategory.Branch;
        public double maximumcash { get; set; } = 20000000.00;
        public double minimumcash { get; set; } = 25.00;
        public bool workstatus { get; set; } = true;
        public DateTime createddate { get; set; } = DateTime.Now;
    }
      public enum BranchCategory
        {
        Branch=1,
        MBranch=2,
        DBranch=3
         }
}
