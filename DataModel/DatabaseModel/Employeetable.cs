﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BankSimulation.DataModel.DatabaseModel
{
    public class Employeetable
    {
        [Required]
        [Key]
        public int employeeid { get; set; }
        [Required]
        [StringLength(20,MinimumLength =2,ErrorMessage ="Employee First Name should be 2-20 characters")]
        [RegularExpression("^[A-Za-z]+$", ErrorMessage = "Name should be only english alphabet.")]
        public string firstname { get; set; } = string.Empty;
        [Required]
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Employee second Name should be 2-20 characters")]
        [RegularExpression("^[A-Za-z]+$",ErrorMessage ="Name should be only english alphabet.")]
        public string secondname { get; set; } = string.Empty;
        [Required]
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Employee Last Name should be 2-20 characters")]
        [RegularExpression("^[A-Za-z]+$", ErrorMessage = "Name should be only english alphabet.")]
        public string lastname{get;set;}=string.Empty;
        [Required]
        public DateTime hiredate { get; set; }=DateTime.Now;
        [Required]
        [Range(1500, 150000)]
        public double salary { get; set; }=15000;
        [Required]
        public int branchid { get; set; }
        [ForeignKey("branchid")]
        public virtual Branchtable ebranch { get; set; } = null!;
        [Required]
        [StringLength(8)]
        [RegularExpression("^(?:male|Male|female|Female)$", ErrorMessage = "Gender can be only Male(male) or Female(female)")]
        public string gender { get; set; } = "Male";
        [Required]
        [StringLength(18)]
        [RegularExpression("^(?:Permanent|Contract|Probation)$", ErrorMessage = "Employement Type can be only Permanent,Probation or Contract")]
        public string etype { get; set; } = "Probation";
        public String? photo { get; set; }
        [Required]
        [StringLength(13, MinimumLength = 10, ErrorMessage = "Phone Numbere should be 10-13 digits")]
        [RegularExpression("^[0-9\\+]+$", ErrorMessage = "Phone Number should contain Only number and (+) ")]
        public string phone { get; set; } = null!;
        [Required]
        public EmployeeRole erole { get; set; } =EmployeeRole.CSOfficer;
        [Required]
        public DateTime birthdate { get; set; } = DateTime.Now.AddMonths(-12*18);
        public DateTime? retiredate { get; set; }
        public DateTime createddate { get; set; }=DateTime.Now;

    }
    public enum EmployeeRole
    {
        Manager=4,
        CSOfficer=1,
        Cashier=2,
        Auditor=3,
    }
}
