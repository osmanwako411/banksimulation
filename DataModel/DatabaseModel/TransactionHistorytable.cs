﻿using BankSimulation.DataModel.DatabaseModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BankSimulation.DataModel
{
    public class TransactionHistorytable
    {
        [Key]
        [StringLength(26, MinimumLength = 26, ErrorMessage = "Transaction Id should be 26 characters.")]
        [RegularExpression("^[A-Z0-9]+$",ErrorMessage ="Transaction Number should be only Capital Letters or Numbers")]
        public string TransactionId { get; set; } = null!;
        [Required]
        [StringLength(13, MinimumLength = 13, ErrorMessage = "Transaction Id should be 26 characters.")]
        [ForeignKey(nameof(TBalance))]
        public string TAccountNo { get; set; } = null!;
        public Transactiontable TBalance { get;set;}=null!;
        [Required]
        public double TransactionAmount { get; set; } = 0.0;
        [Required]
        [Range(0.0, double.MaxValue)]
        public double PreviousBalance { get; set; }=0.0;
        [Required]
        [Range(0.0,double.MaxValue)]
        public double TransactionBalance { get; set; }= 0.0;
        [Required]
        public DateTime TransactionDate { get; set; }= DateTime.Now;
    }
}
