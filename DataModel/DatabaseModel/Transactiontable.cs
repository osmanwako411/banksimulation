﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BankSimulation.DataModel.DatabaseModel
{
    public class Transactiontable
    {
        [Key]
        [StringLength(13,MinimumLength =13,ErrorMessage ="Account Number should be 13 characters.")]
        public string AccountNo { get; set; } = null!;
        [Required]
        [Range(0.0,20000000.00)]
        public double Deposit { get; set; } = 0.00;
        [Required]
        [Range(20.0, 200000)]
        public double Withdrawal { get; set; } = 0.00;
        [Required]
        public double Currentbalance { get; set; } =0.0;
        [Required]
        public DateTime lasttransactiondate { get; set; }= DateTime.UtcNow;
        public DateTime createddate { get; set; }= DateTime.Now;

    }
}
