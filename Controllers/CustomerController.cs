﻿using BankSimulation.DataModel.DatabaseModel;
using BankSimulation.DataModel.RequestModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BankSimulation.Controllers
{
    [Route("api/ab/customer")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly Bankapidbcontext db;
        public CustomerController(Bankapidbcontext _db)
        {
            db = _db;
        }
        [HttpGet("all")]
        public async Task<ActionResult<IEnumerable<Customertable>>> getCustomers()
        {
            var customers=await db.Customertables.Include(b=> b.Branch).Include(c=>c.Book).ToListAsync();
            if (customers == null)
            {
                return NoContent();
            }
            return Ok(customers);
        }
        [HttpGet("search/{id}")]
        public async Task<ActionResult<Customertable>> getCustomer(long id)
        {
            var customer = await db.Customertables.Include(b => b.Branch).Include(c=>c.Book).FirstAsync(a=>a.CustomerId==id);
            if (customer == null)
            {
                return NoContent();
            }
            return Ok(customer);
        }

        [HttpPost("account/create")]
        public async Task<ActionResult<Customertable>> createAccount([FromBody] CustomerRequest reqcustomer)
        {
            var account_no = "100" + RandomDigits();
            var transaction = new Transactiontable()
            {
                AccountNo = account_no,
                Deposit = reqcustomer.Deposit,
                Withdrawal = 0.0,
                Currentbalance = reqcustomer.Deposit,
                createddate = DateTime.Now,
                lasttransactiondate = DateTime.Now,
            };
            var newcustomer = new Customertable()
            {
                FirstName = reqcustomer.FirstName,
                MiddleName = reqcustomer.MiddleName,
                LastName = reqcustomer.LastName,
                Gender = reqcustomer.Gender,
                phone = reqcustomer.phone,
                BranchId = reqcustomer.BranchId,
                AccountNo = account_no,
                Book = transaction,
            };
             db.Customertables.Add(newcustomer);
            try
            {
                await db.SaveChangesAsync();
                return RedirectToAction("getCustomer", new {id=newcustomer.CustomerId});
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            } 
        }
        [HttpPost("account/manage")]
        public async Task<ActionResult<Customertable>> manageAccount([FromBody] Editcustomeraccount reqcustomer)
        {
            var customer = await db.Customertables.FindAsync(reqcustomer.CustomerId);
            if (customer!=null)
            {
                if (reqcustomer.Photo!=null)
                {
                    customer.Photo = reqcustomer.Photo;  
                }
                if (reqcustomer.Signature != null)
                {
                    customer.Signature = reqcustomer.Signature;
                }
                if (reqcustomer.status != null)
                {
                    customer.status = (bool)reqcustomer.status;
                }
                db.Update(customer);
                try
                {
                    await db.SaveChangesAsync();
                    return Ok(customer);
                }catch(Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            return NoContent();
        }
        [HttpPost("profile/edit/{id}")]
        public async Task<ActionResult<Customertable>> editCustomer([FromRoute] long id, [FromBody] Editcustomerprofile resqcustomer)
        {
            var customer=await db.Customertables.FindAsync(id);
            var branch=await db.Branchtables.FindAsync(resqcustomer.BranchId);
            if (customer == null||branch==null)
            {
                return NotFound();
            }
                customer.FirstName = resqcustomer.FirstName;
                customer.LastName = resqcustomer.LastName;
                customer.MiddleName = resqcustomer.MiddleName;
                customer.Gender = resqcustomer.Gender;
                customer.phone = resqcustomer.phone;
                customer.BranchId = resqcustomer.BranchId;
                db.Update(customer);
            try
            {
                await db.SaveChangesAsync();
                return RedirectToAction("getCustomer",new { id = customer.CustomerId });
            }catch(DbUpdateException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("account/close{id}")]
        public async Task<ActionResult<Customertable>> removeCustomer(long id)
        {
            var customer=await db.Customertables.Include(B=>B.Book).FirstAsync(C=>C.CustomerId==id);
            if(customer == null)
            {
            return NotFound() ;
            }
            if (await addToCustomerHistory(customer))
            {
                db.Remove(customer);
                try
                {
                    await db.SaveChangesAsync();
                    return Ok(customer);
                }catch(Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            return Conflict();
        }
        private string RandomDigits()
        {
            var random = new Random();
            string s = string.Empty;
            for (int i = 0; i < 10; i++)
                s = String.Concat(s, random.Next(10).ToString());
            //Console.WriteLine(s);
            return s;
        }
        private async Task<bool> addToCustomerHistory(Customertable customer)
        {
            var customerHistory = new CustomerHistorytable()
            {
              CustomerId = customer.CustomerId,
              FirstName = customer.FirstName,
              LastName = customer.LastName,
              MiddleName = customer.MiddleName,
              Gender = customer.Gender,
              phone=customer.phone,
              status=customer.status,
              BranchId=customer.BranchId,
              Book=customer.Book,
              createddate=customer.createddate
            };
            db.CustomerHistorytables.Add(customerHistory);
            try
            {
               await db.SaveChangesAsync();
                return true;
            }catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
            return false;
            }
        }
    }
}
