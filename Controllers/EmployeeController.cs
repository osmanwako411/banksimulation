﻿using BankSimulation.DataModel.DatabaseModel;
using BankSimulation.DataModel.RequestModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BankSimulation.Controllers
{
    [Route("api/ab/employee")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly Bankapidbcontext db;
        private readonly ILogger<EmployeeController> logger;
        public EmployeeController(Bankapidbcontext _db, ILogger<EmployeeController> _logger)
        {
            db = _db;
            logger = _logger;
        }
        [HttpGet("all")]
        public async Task<ActionResult<IEnumerable<Employeetable>>> getEmployees()
        {
            return await db.Employeetables.Include(b=>b.ebranch).ToListAsync();
        }

        [HttpGet("search/{id}")]
        public async Task<ActionResult<Employeetable>> getEmployee(int id)
        {
            var employee = await db.Employeetables.Include(b=>b.ebranch).FirstAsync(emp=>emp.employeeid==id);
            if(employee == null)
            {
                return NotFound();
            }
            return Ok(employee);
        }

        [HttpPost("register")]
        public async Task<ActionResult<Employeetable>> registeremployee([FromBody] EmployeeRequest remployee)
        {
            var employee = new Employeetable()
            {
                firstname = remployee.firstname,
                secondname = remployee.secondname,
                lastname = remployee.lastname,
                phone = remployee.phone,
                branchid = remployee.branchid,
                gender = remployee.gender,
                etype = remployee.etype,
                erole = (EmployeeRole)remployee.erole,
                birthdate = remployee.birthdate,
                salary = remployee.salary,
                retiredate = remployee.birthdate.AddMonths(12 * 60)
            };
           // Console.WriteLine(employee.Salary);
            await db.Employeetables.AddAsync(employee); //_context.Customers.Add(customer);
            try
            {
                await db.SaveChangesAsync();
                return RedirectToAction("getEmployee", new {id=employee.employeeid});
            }
            catch (DbUpdateException)
            {
                return BadRequest();
            }
        }

        [HttpPost("update/{empid}")]
        public async Task<ActionResult<Employeetable>> updateEmployee([FromRoute]int empid, [FromBody] EmployeeRequest remployee)
        {
            if (EmployeeExists(empid))
            {
                var newemployee = new Employeetable()
                {
                    employeeid = empid,
                    firstname = remployee.firstname,
                    secondname = remployee.secondname,
                    lastname = remployee.lastname,
                    phone = remployee.phone,
                    branchid = remployee.branchid,
                    gender = remployee.gender,
                    etype = remployee.etype,
                    erole = (EmployeeRole)remployee.erole,
                    birthdate = remployee.birthdate,
                    salary = remployee.salary,
                    retiredate = remployee.birthdate.AddMonths(12 * 60)
                };

                db.Entry(newemployee).State = EntityState.Modified;
                try
                {
                    await db.SaveChangesAsync();
                 return  RedirectToAction("getEmployee",new {id=newemployee.employeeid});
                }
                catch (DbUpdateConcurrencyException)
                {
                    return NotFound();
                }
            }
            return NotFound();
        }

        [HttpPost("update/profile")]
        public  async Task<ActionResult<Employeetable>> changeprofile([FromBody] EditEmployeeProfile profile) {
            var employee=await db.Employeetables.FindAsync(profile.employeeid);
            if (employee==null)
            {
                return BadRequest("No any employee with this Id");
            }
            employee.photo = profile.photo;
            db.Update(employee);
            try
            {
                await db.SaveChangesAsync();
                return Ok(employee);
            }catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("delete/{id}")]
        public async Task<ActionResult<Employeetable>> Delete(int id)
        {
            if (EmployeeExists(id))
            {
                var employee=await db.Employeetables.FindAsync(id);
                if(employee != null)
                {
                    db.Employeetables.Remove(employee);
                    await db.SaveChangesAsync();
                    return NoContent();
                }
                return Ok(employee);
            }
            return NotFound();
        }
        private bool EmployeeExists(int id)
        {
            return db.Employeetables.Any(e => e.employeeid == id);
        }
    }
}