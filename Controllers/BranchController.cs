﻿using BankSimulation.DataModel.DatabaseModel;
using BankSimulation.DataModel.RequestModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BankSimulation.Controllers
{
    [Route("api/ab/branch/")]
    [ApiController]
    public class BranchController : ControllerBase
    {
        private readonly Bankapidbcontext db;
        public BranchController(Bankapidbcontext _db)
        {
            db = _db;
        }

        [HttpGet("all")]
        public async Task<ActionResult<IEnumerable<Branchtable>>> getBranches()
        {
            var branches=await db.Branchtables.ToListAsync();
            if (branches == null)
            {
                return NoContent();
            }
            return Ok(branches);
        }

        // GET api/<BranchController>/5
        [HttpGet("search/{id}")]
        public async Task<ActionResult<IEnumerable<Branchtable>>> getBranch([FromRoute]int id)
        {
            var branch=await db.Branchtables.FindAsync(id);
            if(branch == null)
            {
                return NotFound();
            }
            return Ok(branch);
        }

        // POST api/<BranchController>
        [HttpPost("create")]
        public async Task<ActionResult<IEnumerable<Branchtable>>> addBranch([FromBody] AddBranchRequest branchrequest)
        {
          //  Console.WriteLine(branchname);
            var branch = new Branchtable()
            {
                Branchname=branchrequest.Branchname,
                Bcategory= (BranchCategory)branchrequest.Bcategory,
            };
            await db.Branchtables.AddAsync(branch);
            try
            {
                await db.SaveChangesAsync();
                return RedirectToAction("getBranch",new {id=branch.BranchId});
            }catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // PUT api/<BranchController>/5
        [HttpPut("update/{id}")]
        public async Task<ActionResult<Branchtable>> editBranch(int id,[FromBody] EditBranchRequest editrequest)
        {
            if (branchExist(editrequest.BranchId))
            {
             var branch=await db.Branchtables.FindAsync(editrequest.BranchId);
                if (branch==null)
                {
                    return NoContent();
                }
                branch.Bcategory= (BranchCategory)editrequest.Bcategory;
                branch.Branchname= editrequest.Branchname;
                branch.workstatus = editrequest.workstatus==0?false:true;
                db.Update(branch);
                try
                {
                    await db.SaveChangesAsync();
                    return Ok(branch);// RedirectToAction("getBranch",new { id=branch.BranchId });
                }catch(DbUpdateException ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            return NotFound();
        }

        // DELETE api/<BranchController>/5
        [HttpDelete("remove/{id}")]
        public async Task<ActionResult<Branchtable>> removeBranch(int id)
        {
            var branch=await db.Branchtables.FindAsync(id);
            if (branch==null)
            {
            return NotFound();
            }
            try
            {
                db.Branchtables.Remove(branch);
                await db.SaveChangesAsync();
                return Ok(branch);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        private bool branchExist(int id)
        {
            return db.Branchtables.Any(b => b.BranchId == id);
        }
    }
}
