﻿using BankSimulation.DataModel;
using BankSimulation.DataModel.DatabaseModel;
using BankSimulation.DataModel.RequestModel;
using BankSimulation.DataModel.ResponseModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BankSimulation.Controllers
{
    [Route("api/ab/transaction")]
    [ApiController]
    public class TransactionController : ControllerBase
    {
        private readonly Bankapidbcontext db;
        public TransactionController(Bankapidbcontext _db)
        {
            db = _db;
        }

        [HttpGet("recent/all")]
        public async Task<ActionResult<IEnumerable<Transactiontable>>> getAccounts()
        {
            var transactions=await db.Transactiontables.ToListAsync();
            if (transactions == null)
            {
                return NoContent();
            }
            return Ok(transactions);
        }
        [HttpGet("balance/search/{accountno:length(13):long}")]
        public async Task<ActionResult<ResponseTransaction>> getCustomerAccount([FromRoute] string accountno)
        {
            var customer = await db.Customertables.Include(B => B.Branch).Include(T => T.Book).FirstOrDefaultAsync(A => A.AccountNo == accountno);
            if (customer!=null)
            {
                var responsetransaction = new ResponseTransaction()
                {
                    CustomerId = customer.CustomerId,
                    AccountNo = customer.AccountNo,
                    FirstName = customer.FirstName,
                    LastName = customer.LastName,
                    MiddleName = customer.MiddleName,
                    Gender = customer.Gender,
                    CurrentBalance = customer.Book.Currentbalance,
                    Phone = customer.phone,
                    Branch = customer.Branch,
                    createddate = customer.createddate,
                    status = customer.status,
                    iserror = false,
                    Information = "Successfull loaded"
                };
                return Ok(responsetransaction);
            }
            return NoContent();
        }

        [HttpPost("debit/{accountno:length(13):long}")]
        public async Task<ActionResult<CreditTransanction>> transactionDeposit([FromRoute] string accountno, [FromBody] double value)
        {
            var transaction = await db.Transactiontables.FindAsync(accountno);
            var customer = await db.Customertables.FirstOrDefaultAsync(C=>C.AccountNo==accountno);
            if (transaction == null||customer==null)
            {
                return NotFound();
            }
            if (value < 0)
            {
                return BadRequest("You cannot debit negative value to Account.");
            }
            if (value > 2000000000)
            {
                return BadRequest($"The {value} Amount is not possible  debit to account for your branch category.");
            }
            var transhistory = new TransactionHistorytable()
            {
                TransactionId = generateTransactionId(),
                TAccountNo = transaction.AccountNo,
                TransactionAmount = value,
                PreviousBalance = transaction.Currentbalance,
                TransactionBalance = transaction.Currentbalance + value,
            };
            transaction.Currentbalance = transaction.Currentbalance + value;
            transaction.Withdrawal = 0;
            transaction.Deposit = value;
            transaction.lasttransactiondate = DateTime.Now;
            db.Update(transaction);
            try
            {
                await db.SaveChangesAsync();
                db.Add(transhistory);
                await db.SaveChangesAsync();
                return Ok(DepositOrWithdrawal(customer,transhistory));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        private CreditTransanction DepositOrWithdrawal(Customertable customer, TransactionHistorytable transhistory)
        {
            var credittransaction = new CreditTransanction()
            {
                TransactionId=transhistory.TransactionId,
                CustomerId=customer.CustomerId,
                AccountNo=customer.AccountNo,
                AccountStatus = customer.status ? "Active" : "Deactive",
                FullName = $"{customer.FirstName} {customer.MiddleName} {customer.LastName}",
                CreditAmount=transhistory.TransactionAmount,
                PreviousBalance=transhistory.PreviousBalance,
                CurrentBalance=transhistory.TransactionBalance,
                TransactionType=transhistory.TransactionAmount>=0?"Deposit":"Withdrawal",
                TransactionDate=transhistory.TransactionDate,
                Gender=customer.Gender,
            };
            return credittransaction;
        }

        [HttpPost("credit/{accountno:long:length(13)}")]
        public async Task<ActionResult<CreditTransanction>> transactionWithdrawal([FromRoute] string accountno,[FromBody] double value)
        {
            var transaction = await db.Transactiontables.FindAsync(accountno);
            var customer = await db.Customertables.FirstOrDefaultAsync(C=>C.AccountNo==accountno);
            if (transaction==null||customer==null)
            {
                return NotFound();
            }
            if (value < 0) {
                return BadRequest("You cannot credit negative value from Account.");
            }
            if (value >transaction.Currentbalance)
            {
                return BadRequest($"Customer's balance is insufficient for {value} credit amount from Account.");
            }
            var transhistory = new TransactionHistorytable()
            {
                TransactionId = generateTransactionId(),
                TAccountNo = transaction.AccountNo,
                TransactionAmount = value * -1,
                PreviousBalance = transaction.Currentbalance,
                TransactionBalance = transaction.Currentbalance - value,
            };
            transaction.Currentbalance = transaction.Currentbalance-value;
            transaction.Withdrawal = value;
            transaction.Deposit = 0;
            transaction.lasttransactiondate=DateTime.Now;
            db.Update(transaction);
            try
            {
                await db.SaveChangesAsync();
                 db.Add(transhistory);
                await db.SaveChangesAsync();
                return Ok(DepositOrWithdrawal(customer, transhistory));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpPost("rollback/transactionid")]
        public async Task<ActionResult<CreditTransanction>> transactionRollback([FromBody] TransactionIdRequest request)
        {
            var transactionhistory = await db.TransactionHistorytables.Include(T=>T.TBalance).FirstOrDefaultAsync(t => t.TransactionId == request.TransactionId);
            if (transactionhistory == null)
            {
                return NotFound();
            }
            string account_no = transactionhistory.TAccountNo;
            var transaction = await db.Transactiontables.FindAsync(account_no);

            if (transaction==null)
            {
                return BadRequest($"The customer's Account {account_no} number have been Closed");
            }
            double balance = transaction.Currentbalance,amount=transactionhistory.TransactionAmount;
            transaction.Withdrawal = amount>=0 ? amount:0.0;
            transaction.Deposit = amount <0 ? amount*-1 : 0.0;
            transaction.Currentbalance= balance+ (-1*amount);
            transaction.lasttransactiondate=DateTime.Now;
            db.Transactiontables.Update(transaction);
            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException ex)
            {
                return BadRequest(ex.Message);
            }
            var customer = await db.Customertables.FirstOrDefaultAsync(C => C.AccountNo == account_no);
            if (customer == null)
            {
                return NoContent();
            }
            var newtransactionhistory = new TransactionHistorytable()
            {
                TransactionId = generateTransactionId(),
                TAccountNo = account_no,
                TransactionAmount = amount * -1,
                PreviousBalance = balance,
                TransactionBalance = balance + (-1 * amount),
                TransactionDate = DateTime.Now,
            };
            try
            {
                db.TransactionHistorytables.Add(newtransactionhistory);
                await db.SaveChangesAsync();
                return Ok(DepositOrWithdrawal(customer, newtransactionhistory));
            }catch(Exception ex){
                return BadRequest($"create history error {ex.Message}");
            }
        }

        [HttpGet("history/search/{accountno:length(13):long}")]
        public async Task<ActionResult<IEnumerable<TransactionHistorytable>>> TransactionStatement([FromRoute] string accountno)
        {
            var statements=await db.TransactionHistorytables.Include(B=>B.TBalance).Where(T=>T.TAccountNo==accountno).ToListAsync();
            if (statements==null)
            {
                return NoContent();
            }
            return Ok(statements);
        }
        [HttpPost("search/transactionid")]
        public async Task<ActionResult<TransactionHistorytable>> getTransaction([FromBody] TransactionIdRequest ReqTransaction)
        {
            var transhistory=await db.TransactionHistorytables.FindAsync(ReqTransaction.TransactionId);
            if (transhistory == null)
            {
                return NotFound();
            }
            return Ok(transhistory);
        }
        private string generateTransactionId()
        {
            var random = new Random();
            string genTransactionId = DateTime.Today.ToString("yyyyMMdd");//DateTime.Today.ToString("yyyyMMddHHmmss");
            for (int i = 0; i < 9; i++)
            {
                genTransactionId = String.Concat(genTransactionId, letter[random.Next(26)]);
                genTransactionId = String.Concat(genTransactionId, random.Next(10).ToString());
            }

            return genTransactionId;
        }
        
        private static readonly string[] letter = new[]{ "A", "B", "C", "D", "E", "F", "G", "H", "I", "J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
    }
}
