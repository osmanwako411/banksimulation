﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BankSimulation.Migrations
{
    public partial class firstconnection : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Branchtables",
                columns: table => new
                {
                    BranchId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Branchname = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Bcategory = table.Column<int>(type: "int", nullable: false),
                    maximumcash = table.Column<double>(type: "float", nullable: false),
                    minimumcash = table.Column<double>(type: "float", nullable: false),
                    workstatus = table.Column<bool>(type: "bit", nullable: false),
                    createddate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Branchtables", x => x.BranchId);
                });

            migrationBuilder.CreateTable(
                name: "Transactiontables",
                columns: table => new
                {
                    AccountNo = table.Column<string>(type: "nvarchar(13)", maxLength: 13, nullable: false),
                    Deposit = table.Column<double>(type: "float", nullable: false),
                    Withdrawal = table.Column<double>(type: "float", nullable: false),
                    Currentbalance = table.Column<double>(type: "float", nullable: false),
                    lasttransactiondate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    createddate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Transactiontables", x => x.AccountNo);
                });

            migrationBuilder.CreateTable(
                name: "Employeetables",
                columns: table => new
                {
                    employeeid = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    firstname = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    secondname = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    lastname = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    hiredate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    salary = table.Column<double>(type: "float", nullable: false),
                    branchid = table.Column<int>(type: "int", nullable: false),
                    gender = table.Column<string>(type: "nvarchar(8)", maxLength: 8, nullable: false),
                    etype = table.Column<string>(type: "nvarchar(18)", maxLength: 18, nullable: false),
                    photo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    phone = table.Column<string>(type: "nvarchar(13)", maxLength: 13, nullable: false),
                    erole = table.Column<int>(type: "int", nullable: false),
                    birthdate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    retiredate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    createddate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employeetables", x => x.employeeid);
                    table.ForeignKey(
                        name: "FK_Employeetables_Branchtables_branchid",
                        column: x => x.branchid,
                        principalTable: "Branchtables",
                        principalColumn: "BranchId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CustomerHistorytables",
                columns: table => new
                {
                    CustomerHistoryId = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CustomerId = table.Column<long>(type: "bigint", nullable: false),
                    FirstName = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    MiddleName = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    Gender = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    phone = table.Column<string>(type: "nvarchar(13)", maxLength: 13, nullable: true),
                    Photo = table.Column<byte[]>(type: "varbinary(max)", nullable: true),
                    Signature = table.Column<byte[]>(type: "varbinary(max)", nullable: true),
                    BranchId = table.Column<int>(type: "int", nullable: false),
                    BookAccountNo = table.Column<string>(type: "nvarchar(13)", nullable: false),
                    status = table.Column<bool>(type: "bit", nullable: false),
                    createddate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Deactiveddate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerHistorytables", x => x.CustomerHistoryId);
                    table.ForeignKey(
                        name: "FK_CustomerHistorytables_Branchtables_BranchId",
                        column: x => x.BranchId,
                        principalTable: "Branchtables",
                        principalColumn: "BranchId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CustomerHistorytables_Transactiontables_BookAccountNo",
                        column: x => x.BookAccountNo,
                        principalTable: "Transactiontables",
                        principalColumn: "AccountNo",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Customertables",
                columns: table => new
                {
                    CustomerId = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    MiddleName = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    Gender = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    phone = table.Column<string>(type: "nvarchar(13)", maxLength: 13, nullable: true),
                    Photo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Signature = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    BranchId = table.Column<int>(type: "int", nullable: false),
                    AccountNo = table.Column<string>(type: "nvarchar(13)", maxLength: 13, nullable: false),
                    status = table.Column<bool>(type: "bit", nullable: false),
                    createddate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customertables", x => x.CustomerId);
                    table.ForeignKey(
                        name: "FK_Customertables_Branchtables_BranchId",
                        column: x => x.BranchId,
                        principalTable: "Branchtables",
                        principalColumn: "BranchId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Customertables_Transactiontables_AccountNo",
                        column: x => x.AccountNo,
                        principalTable: "Transactiontables",
                        principalColumn: "AccountNo",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TransactionHistorytables",
                columns: table => new
                {
                    TransactionId = table.Column<string>(type: "nvarchar(26)", maxLength: 26, nullable: false),
                    TAccountNo = table.Column<string>(type: "nvarchar(13)", maxLength: 13, nullable: false),
                    TransactionAmount = table.Column<double>(type: "float", nullable: false),
                    PreviousBalance = table.Column<double>(type: "float", nullable: false),
                    TransactionBalance = table.Column<double>(type: "float", nullable: false),
                    TransactionDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransactionHistorytables", x => x.TransactionId);
                    table.ForeignKey(
                        name: "FK_TransactionHistorytables_Transactiontables_TAccountNo",
                        column: x => x.TAccountNo,
                        principalTable: "Transactiontables",
                        principalColumn: "AccountNo",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CustomerHistorytables_BookAccountNo",
                table: "CustomerHistorytables",
                column: "BookAccountNo");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerHistorytables_BranchId",
                table: "CustomerHistorytables",
                column: "BranchId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerHistorytables_CustomerId",
                table: "CustomerHistorytables",
                column: "CustomerId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Customertables_AccountNo",
                table: "Customertables",
                column: "AccountNo");

            migrationBuilder.CreateIndex(
                name: "IX_Customertables_BranchId",
                table: "Customertables",
                column: "BranchId");

            migrationBuilder.CreateIndex(
                name: "IX_Customertables_phone",
                table: "Customertables",
                column: "phone",
                unique: true,
                filter: "[phone] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Employeetables_branchid",
                table: "Employeetables",
                column: "branchid");

            migrationBuilder.CreateIndex(
                name: "IX_Employeetables_phone",
                table: "Employeetables",
                column: "phone",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_TransactionHistorytables_TAccountNo",
                table: "TransactionHistorytables",
                column: "TAccountNo");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CustomerHistorytables");

            migrationBuilder.DropTable(
                name: "Customertables");

            migrationBuilder.DropTable(
                name: "Employeetables");

            migrationBuilder.DropTable(
                name: "TransactionHistorytables");

            migrationBuilder.DropTable(
                name: "Branchtables");

            migrationBuilder.DropTable(
                name: "Transactiontables");
        }
    }
}
