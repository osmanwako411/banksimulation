﻿using BankSimulation.DataModel.DatabaseModel;
using System.Collections.Generic;

namespace BankSimulation.Repository
{
    public interface IEmployeeRepository
    {
        public IEnumerable<Employeetable> getallemployee();
    }
}
